package Homework3;

public class Truck extends Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Truck engine is started");
    }

    @Override
    public void drive() {
        System.out.println("Person is driving truck");
    }
}
