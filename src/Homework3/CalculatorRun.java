package Homework3;

import java.util.Scanner;
public class CalculatorRun {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the first number: ");
        float num1 = sc.nextFloat();

        System.out.print("Enter the second number: ");
        float num2 = sc.nextFloat();

        System.out.print("Enter the type of operation you want to perform (+, -, *, /): ");
        String operator = sc.next();

        Calculator calc = new Calculator(num1, num2);

        float result = 0;

        switch(operator) {

            case "+":
                result = calc.add();
            break;

            case "-":
                result = calc.subtract();
            break;

            case "*":
                result = calc.multiply();
            break;

            case "/":
                if (num2 == 0) {
                    try {
                        calc.divideByZero();

                    } catch(ArithmeticException e) {
                        System.out.println("Be careful. You can not divide the number by 0!!!");

                    }
                }
                result = calc.divide();
                break;

            default:
                System.out.println("INVALID OPERATOR!");
        }

        System.out.println("----------------------------------------------------------------");
        System.out.println("Result is: " + result);

        System.out.println();

    }
}
