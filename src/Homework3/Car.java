package Homework3;

public class Car extends Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Car engine is started");
    }
    @Override
    public void drive() {
        System.out.println("Person is driving car");
    }
}
