package Homework3;

public class Calculator {
  public float firstNumber;
  public float secondNumber;

  public Calculator(float firstNumber, float secondNumber) {
      this.firstNumber = firstNumber;
      this.secondNumber = secondNumber;

  }
  public float add() {
      return firstNumber + secondNumber;
  }

  public float subtract() {
      return firstNumber - secondNumber;
  }

  public float multiply() {
      return firstNumber * secondNumber;
  }

  public float divide() {
      return firstNumber / secondNumber;
  }
    public void divideByZero() {
        throw new ArithmeticException("Not Allowed to divide the number by 0!!!");
    }

}
