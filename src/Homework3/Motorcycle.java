package Homework3;

public class Motorcycle extends Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Motorcycle engine is started");
    }

    @Override
    public void drive() {
        System.out.println("Person is driving motorcycle");
    }
}
