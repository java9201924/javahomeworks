package Homework5.Task2;

public class EmployeeRunner {
    public static void main(String[] args) {

        Manager manager = new Manager("Jack", 2500, "Finance");
        Developer developer = new Developer("Anna", 3600, "Java");

        manager.getDetails();
        System.out.println();
        developer.getDetails();
    }
}
