package Homework5.Task2;

public class Manager extends Employee{
    private String department;
    public Manager(String name, double salary, String department) {
        super(name, salary);
        this.department = department;
    }
//    @Override
//    public void getDetails() {
//        super.getDetails();
//        System.out.println("Department: " + department);
//    }
    @Override
    void getDetails() {
        System.out.println("Name: " + getName());
        System.out.println("Salary: " + getSalary() + "$");
        System.out.println("Department: " + department);
    }
}
