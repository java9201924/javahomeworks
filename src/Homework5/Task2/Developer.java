package Homework5.Task2;

public class Developer extends Employee{
    private String programmingLanguage;
    public Developer(String name, double salary, String programmingLanguage) {
        super(name, salary);
        this.programmingLanguage = programmingLanguage;
    }
//    @Override
//    public void getDetails() {
//        super.getDetails();
//        System.out.println("Programming Language: " + programmingLanguage);
//    }
    @Override
    void getDetails() {
        System.out.println("Name: " + getName());
        System.out.println("Salary: " + getSalary() + "$");
        System.out.println("Programming Language: " + programmingLanguage);
    }
}
