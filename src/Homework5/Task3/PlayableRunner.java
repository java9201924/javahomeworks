package Homework5.Task3;

public class PlayableRunner {
    public static void main(String[] args) {

        AudioPlayer audioPlayer = new AudioPlayer();
        VideoPlayer videoPlayer = new VideoPlayer();

        audioPlayer.play();
        audioPlayer.stop();
        System.out.println("-------------------------------");
        videoPlayer.play();
        videoPlayer.stop();

    }
}
