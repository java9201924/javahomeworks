package Homework5.Task3;

public class VideoPlayer implements Playable{
    @Override
    public void play() {
        System.out.println("Playing videoPlayer...");
    }

    @Override
    public void stop() {
        System.out.println("VideoPlayer is stopped.");
    }
}
