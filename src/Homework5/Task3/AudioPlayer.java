package Homework5.Task3;

public class AudioPlayer implements Playable{
    @Override
    public void play() {
        System.out.println("Playing audioPlayer...");
    }
    @Override
    public void stop() {
        System.out.println("AudioPlayer is stopped.");
    }
}
