package Homework5.Task3;

public interface Playable {
    void play();
    void stop();
}
