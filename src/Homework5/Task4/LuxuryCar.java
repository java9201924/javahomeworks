package Homework5.Task4;

public class LuxuryCar extends Car{
    private String[] premiumFeatures;

    public LuxuryCar(String make, String model, int year, double rentalRate, String[] premiumFeatures) {
        super(make, model, year, rentalRate);
        this.premiumFeatures = premiumFeatures;
    }

    public String[] getPremiumFeatures() {
        return premiumFeatures;
    }

    public void setPremiumFeatures(String[] premiumFeatures) {
        this.premiumFeatures = premiumFeatures;
    }

    @Override
    double calculateRentalCharge(int numDays) {
        return getRentalRate() * numDays * 1.5;
    }

}
