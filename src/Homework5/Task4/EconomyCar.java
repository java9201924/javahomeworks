package Homework5.Task4;

public class EconomyCar extends Car{
    private double fullEfficiency;

    public EconomyCar(String make, String model, int year, double rentalRate, double fullEfficiency) {
        super(make, model, year, rentalRate);
        this.fullEfficiency = fullEfficiency;
    }

    public double getFullEfficiency() {
        return fullEfficiency;
    }

    public void setFullEfficiency(double fullEfficiency) {
        this.fullEfficiency = fullEfficiency;
    }

    @Override
    double calculateRentalCharge(int numDays) {
        return getRentalRate() * numDays;
    }

}
