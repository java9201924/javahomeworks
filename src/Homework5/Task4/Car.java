package Homework5.Task4;

public abstract class Car implements Rentable {
    private String make;
    private String model;
    private int year;
    private double rentalRate;
    private boolean available;

    public Car(String make, String model, int year, double rentalRate) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.rentalRate = rentalRate;
        this.available = true;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRentalRate() {
        return rentalRate;
    }

    public void setRentalRate(double rentalRate) {
        this.rentalRate = rentalRate;
    }
    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    abstract double calculateRentalCharge(int numDays);

    @Override
    public void rent(int numDays) {
        if(available) {
            System.out.println("The car has been rented for " + numDays + " days.");
            setAvailable(false);
        } else {
            System.out.println("The car is not available for rent right now.");
        }

    }
    @Override
    public void returnCar() {
        if(!available) {
            System.out.println("The car has been returned.");
            setAvailable(true);
        } else {
            System.out.println("The car is already available.");
        }

    }
    public void displayCarInfo() {
        System.out.println("Car Info");
        System.out.println("Make: " + make);
        System.out.println("Model: " + model);
        System.out.println("Year: " + year);
        System.out.println("Rental rate: " +rentalRate + "$");
    }
}
