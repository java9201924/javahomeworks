package Homework5.Task4;

public class CarRunner {
    public static void main(String[] args) {

        EconomyCar economyCar = new EconomyCar("Nissan", "Almera", 2015, 50.0, 25.0);
        economyCar.setAvailable(true);

        String[] premiumFeatures = {"Leather seats", "Navigation system", "Sunroof"};
        LuxuryCar luxuryCar = new LuxuryCar("Mercedes-Benz", "S-Class", 2002, 100.0, premiumFeatures);
        luxuryCar.setAvailable(false);


        RentalTransaction transaction1 = new RentalTransaction(economyCar, "David Smith",6);
        RentalTransaction transaction2 = new RentalTransaction(luxuryCar, "Helen Doe", 8);

        transaction1.displayTransactionInfo();
        System.out.println();
        transaction2.displayTransactionInfo();
        System.out.println();
        economyCar.rent(6);
        economyCar.returnCar();
        System.out.println();
        luxuryCar.rent(8);
        luxuryCar.returnCar();

    }
}
