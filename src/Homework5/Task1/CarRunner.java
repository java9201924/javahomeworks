package Homework5.Task1;

public class CarRunner {
    public static void main(String[] args) {

        Car car = new Car();

        car.setMake("Nissan");
        car.setModel("Almera");
        car.setYear(1995);
        car.setRentalPrice(55.0);

        System.out.println("Make: " + car.getMake());
        System.out.println("Model: " + car.getModel());
        System.out.println("Year: " + car.getYear());
        System.out.println("Rental price: " + car.getRentalPrice() + "$");

    }
}
