package Homework6;

public abstract class Room {
    private int roomNumber;
    private double nightlyRate;
    private boolean booked;

    public Room(int roomNumber, double nightlyRate) {
        this.roomNumber = roomNumber;
        this.nightlyRate = nightlyRate;
        this.booked = false;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public double getNightlyRate() {
        return nightlyRate;
    }

    public void setNightlyRate(double nightlyRate) {
        this.nightlyRate = nightlyRate;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    abstract void book();

    abstract boolean checkAvailability();

    abstract double calculateCharges(int numberOfNights);
}
