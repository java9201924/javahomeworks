package Homework6;

public class Suite extends Room{
    private boolean hasLivingRoom;

    public Suite(int roomNumber, double nightlyRate, boolean hasLivingRoom) {
        super(roomNumber, nightlyRate);
        this.hasLivingRoom = hasLivingRoom;
    }

    @Override
    void book() {
        if(checkAvailability()) {
            System.out.println("Suite " + getRoomNumber() + " is booked now.");
            setBooked(true);
        } else {
            System.out.println("Suite " + getRoomNumber() + " is not available right now.");
        }

    }

    @Override
    boolean checkAvailability() {
        return !isBooked();
    }

    @Override
    double calculateCharges(int numberOfNights) {
        return getNightlyRate() * numberOfNights;
    }
}
