package Homework6;

public class DeluxeRoom extends Room{
    private int numberOfBeds;

    public DeluxeRoom(int roomNumber, double nightlyRate, int numberOfBeds) {
        super(roomNumber, nightlyRate);
        this.numberOfBeds = numberOfBeds;
    }

    @Override
    void book() {
        if(checkAvailability()) {
            System.out.println("Deluxe room " + getRoomNumber() + " is booked now.");
            setBooked(true);
        } else {
            System.out.println("Deluxe room " + getRoomNumber() + " is not available right now.");
        }

    }

    @Override
    boolean checkAvailability() {
        return !isBooked();
    }

    @Override
    double calculateCharges(int numberOfNights) {
        return getNightlyRate() * numberOfNights;
    }
}
