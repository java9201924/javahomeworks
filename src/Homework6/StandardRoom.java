package Homework6;

public class StandardRoom extends Room{
    public StandardRoom(int roomNumber, double nightlyRate) {
        super(roomNumber, nightlyRate);
    }

    @Override
    void book() {
        if(checkAvailability()) {
            System.out.println("Standard room " + getRoomNumber() + " booked now.");
            setBooked(true);
        } else {
            System.out.println("Standard room " + getRoomNumber() + " is not available right now");
        }
    }

    @Override
    boolean checkAvailability() {
        return !isBooked();
    }

    @Override
    double calculateCharges(int numberOfNights) {
        return getNightlyRate() * numberOfNights;
    }
}
