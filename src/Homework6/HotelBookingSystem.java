package Homework6;

public class HotelBookingSystem {

    public static void main(String[] args) {

        StandardRoom standardRoom = new StandardRoom(105, 55);
        DeluxeRoom deluxeRoom = new DeluxeRoom(202,120,4);
        Suite suite = new Suite(184,80,true);

        System.out.println("------------------------------------------------------------------");
        standardRoom.book();
        System.out.println("Available: " + standardRoom.checkAvailability());
        System.out.println("Total charges: " + standardRoom.calculateCharges(5));
        System.out.println("------------------------------------------------------------------");
        deluxeRoom.book();
        System.out.println("Available: " + deluxeRoom.checkAvailability());
        System.out.println("Total charges: " + deluxeRoom.calculateCharges(4));
        System.out.println("------------------------------------------------------------------");
        suite.setBooked(true);
        suite.book();
        System.out.println("Available: " + suite.checkAvailability());
        System.out.println("------------------------------------------------------------------");
    }
}
