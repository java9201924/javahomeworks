package Homework7;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        // Create a list of integers and find the maximum and minimum values.

//        ArrayList<Integer> integers = new ArrayList<>();
//        integers.add(8);
//        integers.add(-2);
//        integers.add(3);
//        integers.add(10);
//        integers.add(12);
//        integers.add(9);
//        integers.add(-7);
//        integers.add(0);
//
//        int min = Collections.min(integers);
//        int max = Collections.max(integers);
//
//        System.out.println("Minimum integer value is: " + min);
//        System.out.println("Maximum integer value is: " + max);

        // Remove all elements from a list that are divisible by 3.

//        ArrayList<Integer> integers = new ArrayList<>();
//        integers.add(8);
//        integers.add(-2);
//        integers.add(3);
//        integers.add(10);
//        integers.add(12);
//        integers.add(9);
//        integers.add(-7);
//        integers.add(0);
//
//        ArrayList<Integer> numbersDividedBy3 = new ArrayList<>();
//
//        for(Integer num : integers) {
//            if(num%3==0) {
//                numbersDividedBy3.add(num);
//            }
//        }
//        for(Integer num : numbersDividedBy3) {
//            System.out.print(num + " ");
//        }

        // Check if a list of strings contains a specific element.

//        ArrayList<String> strings = new ArrayList<>();
//        strings.add("Hello");
//        strings.add("Hi");
//        strings.add("Goodbye");
//        strings.add("Salam");
//        strings.add("Privet");
//
//        System.out.println(strings.contains("Hello"));

        // Find the average of all even numbers in a list of integers.

//        List<Integer> integers = new LinkedList<>();
//        integers.add(5);
//        integers.add(20);
//        integers.add(-3);
//        integers.add(7);
//        integers.add(16);
//        integers.add(15);
//        integers.add(9);
//        integers.add(-12);
//
//        int count = 0;
//        int sum = 0;
//
//        for (Integer num : integers) {
//            if (num % 2 == 0) {
//                count++;
//                sum += num;
//            }
//        }
//        System.out.println("Average of even numbers: " + sum/count);

        // Create a HashMap to store inventory items and their quantities.
        // Without class

//        Map<String, Integer> inventoryItems = new HashMap<>();
//        inventoryItems.put("Apple", 10);
//        inventoryItems.put("Orange", 8);
//        inventoryItems.put("Plum", 3);
//        inventoryItems.put("Grape", 5);
//        inventoryItems.replace("Apple", 9);
//        inventoryItems.remove("Orange");
//        System.out.println(inventoryItems.containsKey("Orange"));
//
//        for(Map.Entry<String, Integer> item : inventoryItems.entrySet()) {
//            System.out.println("Key: " + item.getKey() + " " + "Value: " + item.getValue());
//        }

    }
}
