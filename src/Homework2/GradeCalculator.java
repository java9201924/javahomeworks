package Homework2;

import java.util.Scanner;

public class GradeCalculator {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name:");
        String name = scanner.next();
        System.out.println("Hello, dear " + name + "!");
        System.out.println("------------------------------------------");

        int length;
        System.out.println("How many subjects do you have?");
        length = scanner.nextInt();

        System.out.println("Please, write the names of your subjects:");

        String[] subjects = new String[length];

        for(int i=0;i<length;i++) {
            subjects[i] = scanner.next();
        }

        System.out.println("Please, choose one of these subjects: ");
        String subject = scanner.next();

        System.out.println("Now add your grades from this subject (Note: You have to add 5 grades):");

        int[] grades = new int[5];

        for(int i=0;i< grades.length;i++) {
            grades[i] = scanner.nextInt();
        }

        int min = grades[0];

        // Min grade
        for(int i=0;i<grades.length;i++) {
            if(grades[i]<min) {
                min = grades[i];
            }
        }

        int sum = 0;

        // Avg grade
        for(int i=0;i< grades.length;i++) {
            sum += grades[i];
        }
        int avg = sum / 5;

        int max = grades[0];

        // Max grade
        for(int i=0;i<grades.length;i++) {
            if(grades[i]>max) {
                max = grades[i];
            }
        }

        System.out.println("------------------------------------------");
        System.out.println("Min grade for this subject: " + min);
        System.out.println("Avg grade for this subject: " + avg);
        System.out.println("Max grade for this subject: " + max);
        System.out.println("-------------------------------------------");

    }

}
