package Homework2;

public class HomeWork2 {

    public static void main(String[] args) {

        // Homework2 Tasks

        // 1.
//        int number;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Number: ");
//        number = sc.nextInt();
//        System.out.println("---------------------------");
//
//        int fact = 1;
//
//        for(int i=1;i<=number;i++) {
//            fact *= i;
//        }
//
//        System.out.println("Factorial of the Number: " + fact);

        // 2.
//        int number;
//        int sum = 0;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Please, enter a number: ");
//        number = sc.nextInt(); // 156
//
//        while(number!=0) { // 156  // 15 // 1
//            int last_digit = number % 10; // 156%10=6  // 15%10=5 // 1%10=1
//            sum += last_digit; // 0+6+5+1
//            number /= 10; // number = 156/10 = 15 // 15/10 = 1 // 1/10 = 0
//        }
//
//        System.out.println("Sum of the digits of the number: " + sum);

        // 3.
//        int number;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Please, enter a number: ");
//        number = sc.nextInt();
//
//        int reversedNumber = 0;
//
//        while(number!=0) { // 123 // 12 // 1
//            int last_digit = number % 10; // 3 // 2 // 1
//            reversedNumber = reversedNumber*10 + last_digit; // 0*123 + 3 // 3*10 + 2 // 32*10 +1= 321
//            number/=10; // 123/10=12 // 1 // 0
//        }
//
//        System.out.println("Reversed number: " + reversedNumber);

        // 4.
//        int length;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Enter the length of the array: ");
//        length = sc.nextInt();
//        System.out.println("Enter the elements of the array");
//        System.out.println("----------------------------------------");
//
//        int[] numbers = new int[length];
//
//        for(int i=0;i<length;i++) {
//            numbers[i] = sc.nextInt();
//        }
//
//        int min = numbers[0];
//
//        for(int i=0;i<numbers.length;i++) {
//            if(min>numbers[i]) {
//                min = numbers[i];
//            }
//        }
//
//        System.out.println("----------------------------------------");
//        System.out.println("Minimum element of an array: " + min);

        // 5.
//        int length;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Enter the length of an array: ");
//        length = sc.nextInt();
//        System.out.println("Enter elements of an array");
//        System.out.println("--------------------------------------");
//
//        int[] numbers = new int[length];
//
//        for (int i = 0; i < length; i++) {
//            numbers[i] = sc.nextInt();
//        }
//
//        int min = numbers[0];
//        int index = 0;
//
//        for (int i = 0; i < numbers.length; i++) {
//            if (numbers[i] < min) {
//                min = numbers[i];
//                index = i;
//            }
//        }
//
//        System.out.println("------------------------------------------------");
//        System.out.println("Index of the minimum element of an array: " + index);

        // 6.
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Length of array: ");
//        int length = sc.nextInt();
//        System.out.println("Enter the elements of the array");
//        System.out.println("-------------------------------------");
//
//        int[] numbers = new int[length];
//
//        for (int i = 0; i < length; i++) {
//            numbers[i] = sc.nextInt();
//        }
//
//        int sum = 0;
//
//        // Calculate the sum of elements with odd indexes
//        for (int i = 0; i < numbers.length; i += 1) {
//            if(i % 2 != 0) {
//                sum += numbers[i];
//            }
//        }
//
//        System.out.println("------------------------------------------");
//        System.out.println("Sum of elements with odd indexes: " + sum);

        // 7.
//        int length;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("The length of an array: ");
//        length = sc.nextInt();
//        System.out.println("Elements of an array:");
//
//        int[] numbers = new int[length];
//
//        for(int i=0;i<length;i++) {
//            numbers[i] = sc.nextInt();
//        }
//
//        for(int i=0;i<length/2;i++) {
//            int temp = numbers[i];
//            numbers[i] = numbers[length-1-i];
//            numbers[length-1-i] = temp;
//        }
//
//        System.out.print("Reversed array: ");
//
//        for(int num : numbers) {
//            System.out.print(num + " ");
//        }

        // 8.
//        int length;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Enter the length of array: ");
//        length = sc.nextInt();
//        System.out.println("----------------------------------");
//        System.out.println("Enter the elements of the array:");
//        System.out.println("----------------------------------");
//
//        int[] numbers = new int[length];
//
//        for(int i=0;i<length;i++) {
//            numbers[i] = sc.nextInt();
//        }
//
//        int count = 0;
//
//        for(int i=0;i<numbers.length;i++) {
//            if(numbers[i]%2==1) {
//                count+=1;
//            }
//        }
//
//        System.out.println("-------------------------------------------------------");
//        System.out.println("The number of odd elements in the array: " + count);
//        System.out.println("-------------------------------------------------------");

        // 9.
//        int length;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("The length of an array: ");
//        length = sc.nextInt();
//        System.out.println("Elements of an array: ");
//        System.out.println("--------------------------------");
//
//        int[] numbers = new int[length];
//
//        for(int i=0;i<length;i++) {
//            numbers[i] = sc.nextInt();
//        }
//
//        int mid = length/2;
//
//        for(int i=0;i<mid;i++) {
//            int temp = numbers[i];
//            numbers[i] = numbers[mid+i];
//            numbers[mid+i] = temp;
//        }
//
//        System.out.println("--------------------------------");
//        System.out.print("Modified array: ");
//        for(int num : numbers) {
//            System.out.print(num + " ");
//        }

    }


}
