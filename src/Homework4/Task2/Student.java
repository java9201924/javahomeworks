package Homework4.Task2;

public class Student extends Person{
    private String studentID;

    public Student(String name, int age, String studentID) {
        super(name, age);
        this.studentID = studentID;
    }
    @Override
    void displayInfo() {
        System.out.println("Student's info");
        System.out.println("-------------------------");
        System.out.println("Name: " + getName());
        System.out.println("Age: " + getAge());
        System.out.println("Student ID: " + studentID);
    }
}
