package Homework4.Task2;

public class PersonRunner {
    public static void main(String[] args) {

        Student student = new Student("John", 16, "101");
        Teacher teacher = new Teacher("Helen", 38, "History");

        student.displayInfo();
        System.out.println();
        teacher.displayInfo();
    }
}
