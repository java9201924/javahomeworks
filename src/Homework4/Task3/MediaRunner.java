package Homework4.Task3;

public class MediaRunner {
    public static void main(String[] args) {

        Music music = new Music();
        Movie movie = new Movie();

        music.play();
        movie.play();
    }
}
