package Homework4.Task4;

import java.io.FileWriter;
import java.io.IOException;

public class FileLogger implements Logger{
    private String fileName;

    public FileLogger(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void logInfo(String message) {
        writeToFile("INFO: " + message);
    }

    @Override
    public void logWarning(String message) {
        writeToFile("WARNING: " + message);
    }

    @Override
    public void logError(String message) {
        writeToFile("ERROR: " + message);
    }

    public void writeToFile(String message) {
        try(FileWriter fw = new FileWriter(fileName, true)) {
            fw.write(message);
            fw.close();
            System.out.println("Messages were successfully written to the file");
        } catch(IOException e) {
            System.out.println("An error occurred!");
            e.printStackTrace();
        }

    }
}
