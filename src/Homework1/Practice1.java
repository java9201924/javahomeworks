package Homework1;

public class Practice1 {

    public static void main(String[] args) {

        // PRACTICE

        // 1.
//        String name;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Your name: ");
//        name = sc.nextLine();
//        System.out.println("Hello, " + name + " :)");

        // 2.
//        int number;
//        Scanner input = new Scanner(System.in);
//        System.out.print("Please, enter a number: ");
//        number = input.nextInt();
//        if(number % 2 == 0 && number % 3 == 0) {
//            System.out.println("Number is both divisible by 2 and 3");
//        } else {
//            System.out.println("Number is not divisible by 2 and 3");
//        }

        // 3.
//        int firstNumber, secondNumber;
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Please, enter the numbers: ");
//        firstNumber = scanner.nextInt();
//        secondNumber = scanner.nextInt();
//        if(firstNumber == secondNumber) {
//            System.out.println("Numbers are equal");
//        } else {
//            System.out.println("Numbers are not equal");
//        }

        // 4.
//       int number;
//       Scanner scanner = new Scanner(System.in);
//       System.out.print("Please, enter a number: ");
//       number = scanner.nextInt();
//       if(number>0) {
//           System.out.println("Number is positive");
//       } else if(number == 0) {
//           System.out.println("Number is zero");
//       } else {
//           System.out.println("Number is negative");
//       }

        // 5.
//        int year;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Please, enter a year: ");
//        year = sc.nextInt();
//        if(year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
//            System.out.println(year + " is a leap year");
//        } else {
//            System.out.println(year + " is not a leap year");
//        }

        // 6.
//        int celsius, fahrenheit;
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Enter temperature in Celsius: ");
//        celsius = sc.nextInt();
//        fahrenheit = (celsius * 9/5) + 32;
//        System.out.println(celsius + " Celsius is " + fahrenheit + " Fahrenheit");

        // 7.
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Enter the first number: ");
//        int num1 = sc.nextInt();
//        System.out.print("Enter the second number: ");
//        int num2 = sc.nextInt();
//        System.out.print("Enter the third number: ");
//        int num3 = sc.nextInt();
//
//        int largest;
//
//        if(num1 >= num2 && num1 >= num3) {
//            largest = num1;
//        } else if(num2 >= num1 && num2 >= num3) {
//            largest = num2;
//        } else {
//            largest = num3;
//        }
//
//        System.out.print("The largest number is: " + largest);

        // 8.
//        Scanner sc = new Scanner(System.in);
//        System.out.print("The length of rectangle: ");
//        int length = sc.nextInt();
//        System.out.print("The width of rectangle: ");
//        int width = sc.nextInt();
//
//        int area = length * width;
//
//        System.out.print("The area of rectangle is: " + area);

    }
}
