package Homework1;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        double num1, num2, result = 0;

        System.out.println("--------------------------------------------------");
        System.out.println("SIMPLE CALCULATOR");
        System.out.println("--------------------------------------------------");

        System.out.println("Enter the numbers");
        System.out.println("--------------------------------------------------");

        Scanner sc = new Scanner(System.in);

        System.out.print("First number is: ");
        num1 = sc.nextDouble();

        System.out.print("Second number is: ");
        num2 = sc.nextDouble();

        System.out.println("-------------------------------------------------");
        System.out.println("Enter the operator (+,-,*,/)");
        System.out.println("-------------------------------------------------");

        char op = sc.next().charAt(0);
        System.out.println("------------------------------------------------");

        System.out.println("Operator you've chosen is: " + op);
        System.out.println("------------------------------------------------");

        switch (op) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
            default:
                System.out.println("You entered wrong input!");
        }

        System.out.print("RESULT is: ");
        System.out.println(num1 + " " + op + " " + num2 + " = " + result);
        System.out.println("----------------------------------------------------");

    }
}
